package beans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.hibernate.Query;
import org.hibernate.Session;

import entities.Patient;
import entities.RendezVous;
import util.HibernateUtil;

@ManagedBean(name = "deplacerRdvBean", eager = true)
@SessionScoped
public class DeplacerRdvBean {
	@ManagedProperty("#{loginBean}")
	private Login login;
	private String dateDuRendezVous;
	private Date nouveauRendezVous;
	private Date currentDate = new Date();
	private String msg = "";

	
	/**
	 * 
	 */
	public DeplacerRdvBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param login
	 * @param dateDuRendezVous
	 * @param nouveauRendezVous
	 * @param currentDate
	 */
	public DeplacerRdvBean(Login login, String dateDuRendezVous, Date nouveauRendezVous, Date currentDate) {
		super();
		this.login = login;
		this.dateDuRendezVous = dateDuRendezVous;
		this.nouveauRendezVous = nouveauRendezVous;
		this.currentDate = currentDate;
	}

	/**
	 * @return the nouveauRendezVous
	 */
	public Date getNouveauRendezVous() {
		return nouveauRendezVous;
	}

	/**
	 * @param nouveauRendezVous the nouveauRendezVous to set
	 */
	public void setNouveauRendezVous(Date nouveauRendezVous) {
		this.nouveauRendezVous = nouveauRendezVous;
	}

	/**
	 * @return the login
	 */
	public Login getLogin() {
		return login;
	}

	/**
	 * @param login the login to set
	 */
	public void setLogin(Login login) {
		this.login = login;
	}

	/**
	 * @return the dateDuRendezVous
	 */
	public String getDateDuRendezVous() {
		Patient patient = null;

		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			String hql = "from Patient l where l.nom = :nom ";
			Query<Patient> query = session.createQuery(hql);
			List<Patient> patients = query.setParameter("nom", login.getNom()).list();

			if (patients.size() > 0) {
				patient = patients.get(0);
			}
			session.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}

		try {
			if (patient != null) {
				DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
				RendezVous rendezVous = patient.getRendezvous().stream().max(Comparator.comparing(RendezVous::getId))
						.orElseThrow(NoSuchElementException::new);
				;
				dateDuRendezVous = formatter.format(rendezVous.getJourAndCrenaux());
			}
			
		}catch (NoSuchElementException e) {
			msg= "Veuillez prendre d�j� un rendez vous";
			
		}
		

		return dateDuRendezVous;
	}
	

	/**
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * @param msg the msg to set
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * @param dateDuRendezVous the dateDuRendezVous to set
	 */
	public void setDateDuRendezVous(String dateDuRendezVous) {
		this.dateDuRendezVous = dateDuRendezVous;
	}

	/**
	 * @return the currentDate
	 */
	public Date getCurrentDate() {
		return currentDate;
	}

	/**
	 * @param currentDate the currentDate to set
	 */
	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}
	
	public String enregistrerRendezVous() {
		//1 on va cherche le patient dont le nom est r�cup�r� depuis le bean session
		//2 cr�er un rendez vous (Objet) avec la date r�cup�r�e depuis l'IHM
		//3 Mettre � jour le patient r�cup�r� en 1
		//4 Stocker le patient � nouveau dans la base.

		String goToPage = "pageDeConfirmation";
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			String hql = "from Patient l where l.nom = :nom ";
			Query<Patient> query = session.createQuery(hql);
			List<Patient> patients = query.setParameter("nom", login.getNom()).list();
			if(patients.size()>0) {
				Patient patient = patients.get(0);
				
				if (patient != null) {
					RendezVous rendezVous = patient.getRendezvous().stream().max(Comparator.comparing(RendezVous::getId))
							.orElseThrow(NoSuchElementException::new);
					rendezVous.setJourAndCrenaux(nouveauRendezVous);
				}
				session.update(patient);
			}
			session.getTransaction().commit();
			
		} catch (Exception e) {
			goToPage = "resterIci";
			e.printStackTrace();
		}finally {
			session.close();
		}

		return goToPage;
	}

}
