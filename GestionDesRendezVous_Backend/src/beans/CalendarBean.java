package beans;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;


	  @ManagedBean(name = "calendarBean", eager = true)
		@SessionScoped
		public class CalendarBean {
			private int id;
	    	private String annee;
			private String mois;
			private String jours;
			/**
			 * 
			 */
			public CalendarBean() {
				super();
			}
			/**
			 * @param id
			 * @param annee
			 * @param mois
			 * @param jours
			 */
			public CalendarBean(int id, String annee, String mois, String jours) {
				super();
				this.id = id;
				this.annee = annee;
				this.mois = mois;
				this.jours = jours;
			}
			/**
			 * @return the id
			 */
			public int getId() {
				return id;
			}
			/**
			 * @param id the id to set
			 */
			public void setId(int id) {
				this.id = id;
			}
			/**
			 * @return the annee
			 */
			public String getAnnee() {
				return annee;
			}
			/**
			 * @param annee the annee to set
			 */
			public void setAnnee(String annee) {
				this.annee = annee;
			}
			/**
			 * @return the mois
			 */
			public String getMois() {
				return mois;
			}
			/**
			 * @param mois the mois to set
			 */
			public void setMois(String mois) {
				this.mois = mois;
			}
			/**
			 * @return the jours
			 */
			public String getJours() {
				return jours;
			}
			/**
			 * @param jours the jours to set
			 */
			public void setJours(String jours) {
				this.jours = jours;
			}
			
}
