package beans;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import org.hibernate.Query;
import org.hibernate.Session;

import entities.Patient;
import entities.RendezVous;
import util.HibernateUtil;

/**
 * 
 */

/**
 * @author sarak
 *
 */
@ManagedBean(name = "pagePrendreRendezVous", eager = true)
public class PagePrendreRendezVous {
	
	@ManagedProperty("#{loginBean}")
	private Login login;
	
	private Date dateDuRendezVous;
	private String nomPatient;
	private List<String> datesIndisponibles;
	private Date currentDate = new Date();
	
	
	
	/**
	 * @param dateDuRendezVous
	 * @param nomPatient
	 * @param datesIndisponibles
	 */
	public PagePrendreRendezVous(Date dateDuRendezVous, String nomPatient, List<String> datesIndisponibles) {
		super();
		this.dateDuRendezVous = dateDuRendezVous;
		this.nomPatient = nomPatient;
		this.datesIndisponibles = datesIndisponibles;
	}
	/**
	 * 
	 */
	public PagePrendreRendezVous() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @return the dateDuRendezVous
	 */
	public Date getDateDuRendezVous() {
		return dateDuRendezVous;
	}
	/**
	 * @param dateDuRendezVous the dateDuRendezVous to set
	 */
	public void setDateDuRendezVous(Date dateDuRendezVous) {
		this.dateDuRendezVous = dateDuRendezVous;
	}
	/**
	 * @return the nomPatient
	 */
	public String getNomPatient() {
		nomPatient = this.login.getNom();
		return nomPatient;
	}
	/**
	 * @param nomPatient the nomPatient to set
	 */
	public void setNomPatient(String nomPatient) {
		this.nomPatient = nomPatient;
	}
	
	
	/**
	 * @return the datesIndisponibles
	 */
	public List<String> getDatesIndisponibles() {
		datesIndisponibles = new ArrayList();
        DateFormat formatter = new SimpleDateFormat("M-dd-yyyy");

		datesIndisponibles.add(formatter.format(new Date()));
		return datesIndisponibles;
	}
	/**
	 * @param datesIndisponibles the datesIndisponibles to set
	 */
	public void setDatesIndisponibles(List<String> datesIndisponibles) {
		this.datesIndisponibles = datesIndisponibles;
	}
	
	
	/**
	 * @return the login
	 */
	public Login getLogin() {
		return login;
	}
	/**
	 * @param login the login to set
	 */
	public void setLogin(Login login) {
		this.login = login;
	}
	
	
	/**
	 * @return the currentDate
	 */
	public Date getCurrentDate() {
		return currentDate;
	}
	/**
	 * @param currentDate the currentDate to set
	 */
	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}
	public String enregistrerRendezVous() {
		//1 on va cherche le patient dont le nom est r�cup�r� depuis le bean session
		//2 cr�er un rendez vous (Objet) avec la date r�cup�r�e depuis l'IHM
		//3 Mettre � jour le patient r�cup�r� en 1
		//4 Stocker le patient � nouveau dans la base.

		String goToPage = "pageDeConfirmation";
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			String hql = "from Patient l where l.nom = :nom ";
			Query<Patient> query = session.createQuery(hql);
			List<Patient> patients = query.setParameter("nom", login.getNom()).list();
			System.out.println(patients);
			if(patients.size()>0) {
				Patient patient = patients.get(0);
				RendezVous rdv = new RendezVous();
				rdv.setJourAndCrenaux(dateDuRendezVous);
				patient.getRendezvous().add(rdv);
				session.update(patient);
			}
			session.getTransaction().commit();
			
		} catch (Exception e) {
			goToPage = "resterIci";
			e.printStackTrace();
		}finally {
			session.close();
		}

		return goToPage;
	}

}
