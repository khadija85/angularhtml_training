package beans;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 * 
 */

/**
 * @author sarak
 *
 */
    @ManagedBean(name = "therapeuteBean", eager = true)
	@SessionScoped
	public class TherapeuteBean {
		private int id;
    	private String nom;
		private String motdepass;
		/**
		 * 
		 */
		public TherapeuteBean() {
			super();
		}
		/**
		 * @param id
		 * @param nom
		 * @param motdepass
		 */
		public TherapeuteBean(int id, String nom, String motdepass) {
			super();
			this.id = id;
			this.nom = nom;
			this.motdepass = motdepass;
		}
		/**
		 * @return the id
		 */
		public int getId() {
			return id;
		}
		/**
		 * @param id the id to set
		 */
		public void setId(int id) {
			this.id = id;
		}
		/**
		 * @return the nom
		 */
		public String getNom() {
			return nom;
		}
		/**
		 * @param nom the nom to set
		 */
		public void setNom(String nom) {
			this.nom = nom;
		}
		/**
		 * @return the motdepass
		 */
		public String getMotdepass() {
			return motdepass;
		}
		/**
		 * @param motdepass the motdepass to set
		 */
		public void setMotdepass(String motdepass) {
			this.motdepass = motdepass;
		}
		
		
		}
		
