package beans;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.hibernate.Session;
import org.hibernate.Transaction;

import entities.Patient;
import util.HibernateUtil;

/**
 * 
 */

/**
 * @author sarak
 *
 */
@ManagedBean(name = "loginBean", eager = true)
@SessionScoped
public class Login {
	private String nom;
	private String motdepass;

	/**
	 * @param nom
	 * @param motdepass
	 */
	/**
	 * 
	 */
	public Login() {
		super();
	}

	/**
	 * @param nom
	 * @param motdepass
	 */
	public Login(String nom, String motdepass) {
		super();
		this.nom = nom;
		this.motdepass = motdepass;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the motdepasse
	 */
	public String getMotdepass() {
		return motdepass;
	}

	/**
	 * @param motdepasse the motdepasse to set
	 */
	public void setMotdepass(String motdepass) {
		this.motdepass = motdepass;
	}

	public String valider() {

		String goToPage = "welcome";

		try (Session session = HibernateUtil.getSessionFactory().openSession()) {

			String hql = "from Patient l where l.nom = :nom and l.motdepass = :motdepass";
			List result = session.createQuery(hql).setParameter("nom", nom).setParameter("motdepass", motdepass).list();
			if (result.size() == 0) {
				goToPage = "login";
			}
		} catch (Exception e) {
			goToPage = "login";
			e.printStackTrace();
		}

		return goToPage;
	}

}
