package beans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.hibernate.Query;
import org.hibernate.Session;

import entities.Patient;
import entities.RendezVous;
import util.HibernateUtil;

@ManagedBean(name = "confirmationBean", eager = true)
@SessionScoped
public class ConfirmationBean {
	@ManagedProperty("#{loginBean}")
	private Login login;
	
	private String dateDuRendezVous;
	private String nomPatient;
	/**
	 * @param login
	 * @param dateDuRendezVous
	 * @param nomPatient
	 */
	public ConfirmationBean(Login login, String dateDuRendezVous, String nomPatient) {
		super();
		this.login = login;
		this.dateDuRendezVous = dateDuRendezVous;
		this.nomPatient = nomPatient;
	}
	/**
	 * 
	 */
	public ConfirmationBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @return the login
	 */
	public Login getLogin() {
		return login;
	}
	/**
	 * @param login the login to set
	 */
	public void setLogin(Login login) {
		this.login = login;
	}
	/**
	 * @return the dateDuRendezVous
	 */
	public String getDateDuRendezVous() {
				Patient patient = null;
				
				Session session = HibernateUtil.getSessionFactory().openSession();
				try {
					session.beginTransaction();
					String hql = "from Patient l where l.nom = :nom ";
					Query<Patient> query = session.createQuery(hql);
					List<Patient> patients = query.setParameter("nom", login.getNom()).list();
					
					if(patients.size()>0) {
						patient = patients.get(0);
					}
					session.getTransaction().commit();
					
				} catch (Exception e) {
					e.printStackTrace();
				}finally {
					session.close();
				}
		
		
		if(patient != null) {
			 DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			 RendezVous rendezVous = patient.getRendezvous().stream()
		      .max(Comparator.comparing(RendezVous::getId)).orElseThrow(NoSuchElementException::new);;
			dateDuRendezVous = formatter.format(rendezVous.getJourAndCrenaux());
		}
		
		
		return dateDuRendezVous;
	}
	/**
	 * @param dateDuRendezVous the dateDuRendezVous to set
	 */
	public void setDateDuRendezVous(String dateDuRendezVous) {
		this.dateDuRendezVous = dateDuRendezVous;
	}
	/**
	 * @return the nomPatient
	 */
	public String getNomPatient() {
		return nomPatient;
	}
	/**
	 * @param nomPatient the nomPatient to set
	 */
	public void setNomPatient(String nomPatient) {
		this.nomPatient = nomPatient;
	}
	
	
}
