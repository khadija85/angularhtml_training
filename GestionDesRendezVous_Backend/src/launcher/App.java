/**
 * 
 */
package launcher;



import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import entities.Patient;
import util.HibernateUtil;

/**
 * @author sarak
 *
 */
public class App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Patient patient = new Patient("khadija", "koko", null);

		Transaction transaction = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			// start a transaction
			transaction = session.beginTransaction();
			// save the patient objects
			//session.save(patient);

			// commit transaction
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}

		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			List<Patient> patients = session.createQuery("from Patient", Patient.class).list();
			patients.forEach(s -> System.out.println(s.getNom()));
		} catch (Exception e) {
			if (transaction != null) {
				transaction.rollback();
			}
			e.printStackTrace();
		}

	}
}
