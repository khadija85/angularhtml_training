/**
 * 
 */
package entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author sarak
 *
 */
@Entity
@Table(name = "rendezvous")
public class RendezVous {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "jourandcrenaux")
	private Date jourAndCrenaux;

	/**
	 * 
	 */
	public RendezVous() {
		super();
	}

	/**
	 * @param id
	 * @param jourAndCrenaux
	 */
	public RendezVous(int id, Date jourAndCrenaux) {
		super();
		this.id = id;
		this.jourAndCrenaux = jourAndCrenaux;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the jourAndCrenaux
	 */
	public Date getJourAndCrenaux() {
		return jourAndCrenaux;
	}

	/**
	 * @param jourAndCrenaux the jourAndCrenaux to set
	 */
	public void setJourAndCrenaux(Date jourAndCrenaux) {
		this.jourAndCrenaux = jourAndCrenaux;
	}

	@Override
	public String toString() {
		return "RendezVous [id=" + id + ", jourAndCrenaux=" + jourAndCrenaux + "]";
	}
	
	

}
