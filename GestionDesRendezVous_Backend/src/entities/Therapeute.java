/**
 * 
 */
package entities;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author sarak
 *
 */
@Entity
@Table(name = "therapeute")
public class Therapeute {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "nom")
	private String nom;
	
	@Column(name = "motdepass")
	private String motdepass;
	
	/**
	 * 
	 */
	public Therapeute() {
		super();
	}

	public Therapeute (int id, String nom, String motdepass) {
		super();
		this.id = id;
		this.nom = nom;
		this.motdepass = motdepass;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the motdepass
	 */
	public String getMotdepass() {
		return motdepass;
	}

	/**
	 * @param motdepass the motdepass to set
	 */
	public void setMotdepass(String motdepass) {
		this.motdepass = motdepass;
	}
	
}
