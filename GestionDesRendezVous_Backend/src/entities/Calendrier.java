/**
 * 
 */
package entities;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.Table;

/**
 * @author sarak
 *
 */
@Entity
@Table(name = "calendar")
public class Calendrier {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "annee")
	private String annee;
	
	@Column(name = "mois")
	private String mois;
	
	@Column(name = "jour")
	private String jour;
	
	
	
	/**
	 * 
	 */
	public Calendrier() {
		super();
	}



	public Calendrier (int id, String annee, String mois, String jour) {
		super();
		this.id = id;
		this.annee = annee;
		this.mois = mois;
		this.jour = jour;
	}



	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}



	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}



	/**
	 * @return the annee
	 */
	public String getAnnee() {
		return annee;
	}



	/**
	 * @param annee the annee to set
	 */
	public void setAnnee(String annee) {
		this.annee = annee;
	}



	/**
	 * @return the mois
	 */
	public String getMois() {
		return mois;
	}



	/**
	 * @param mois the mois to set
	 */
	public void setMois(String mois) {
		this.mois = mois;
	}



	/**
	 * @return the jour
	 */
	public String getJour() {
		return jour;
	}



	/**
	 * @param jour the jour to set
	 */
	public void setJour(String jour) {
		this.jour = jour;
	}
	
}
