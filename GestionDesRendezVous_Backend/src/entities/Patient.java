/**
 * 
 */
package entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author sarak
 *
 */
@Entity
@Table(name = "patient")
public class Patient {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "nom")
	private String nom;
	
	@Column(name = "motdepass")
	private String motdepass;
	
    @OneToMany(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
	private List<RendezVous> rendezvous;

	/**
	 * 
	 */
	public Patient() {
		super();
	}

	/**
	 * @param nom
	 * @param motdepass
	 * @param rendezvous
	 */
	public Patient(String nom, String motdepass, List<RendezVous> rendezvous) {
		super();
		this.nom = nom;
		this.motdepass = motdepass;
		this.rendezvous = rendezvous;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the motdepass
	 */
	public String getMotdepass() {
		return motdepass;
	}

	/**
	 * @param motdepass the motdepass to set
	 */
	public void setMotdepass(String motdepass) {
		this.motdepass = motdepass;
	}

	/**
	 * @return the rendezvous
	 */
	public List<RendezVous> getRendezvous() {
		return rendezvous;
	}

	/**
	 * @param rendezvous the rendezvous to set
	 */
	public void setRendezvous(List<RendezVous> rendezvous) {
		this.rendezvous = rendezvous;
	}

	@Override
	public String toString() {
		return "Patient [id=" + id + ", nom=" + nom + ", motdepass=" + motdepass + ", rendezvous=" + rendezvous + "]";
	}
	
	public static void name() {
		
	}

}
