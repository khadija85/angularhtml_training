export class Patient {
  constructor(id: number, nom: string, password: string, email: string, sex: string, symtopms: string) {
    this._id = id;
    this._nom = nom;
    this._password = password;
    this._email = email;
    this._sex = sex;
    this._symtopms = symtopms;
  }


  private _id: number;
  private _nom: string;
  private _password: string;
  private _email: string;
  private _sex: string;
  private _symtopms: string;

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get nom(): string {
    return this._nom;
  }

  set nom(value: string) {
    this._nom = value;
  }

  get password(): string {
    return this._password;
  }

  set password(value: string) {
    this._password = value;
  }

  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }

  get sex(): string {
    return this._sex;
  }

  set sex(value: string) {
    this._sex = value;
  }

  get symtopms(): string {
    return this._symtopms;
  }

  set symtopms(value: string) {
    this._symtopms = value;
  }
}


