import {Component, OnInit} from '@angular/core';
import { NgForm } from '@angular/forms';




@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.css']
})
export class PatientComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {

  }
  onFormSubmit(userForm:NgForm) {

    console.log(userForm);
  }

  resetUserForm(userForm: NgForm) {
    userForm.resetForm();
  }







}


