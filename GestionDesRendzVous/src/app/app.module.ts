import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RDVComponent } from './component/rdv/rdv.component';
import { CalendarComponent } from './component/calendar/calendar.component';
import { TherapeuteComponent } from './component/therapeute/therapeute.component';
import { PatientComponent } from './component/patient/patient.component';


@NgModule({
  declarations: [
    AppComponent,
    PatientComponent,
    RDVComponent,
    CalendarComponent,
    TherapeuteComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
