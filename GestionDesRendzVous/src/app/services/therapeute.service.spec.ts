import { TestBed } from '@angular/core/testing';

import { TherapeuteService } from './therapeute.service';

describe('TherapeuteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TherapeuteService = TestBed.get(TherapeuteService);
    expect(service).toBeTruthy();
  });
});
